const fetch = require('node-fetch');

// quick temp test, run node example first

console.time(1);
console.time(2);
fetch('http://localhost:3000/5000').then(() => console.timeEnd(1));
setTimeout(() => fetch('http://localhost:3000/5000').then(() => console.timeEnd(2)), 2000);
// ^ this will take ~5s instead of 7s

// const delay = (t) => new Promise(resolve => setTimeout(resolve, t));

// (async () => {

//   console.time('test');
//   const ts = await Promise.all([

//     fetch(`http://localhost:3000/5000`).then(r => r.text()),

//     delay(100).then(() => fetch(`http://localhost:3000/5000`)).then(r => r.text()),

//     delay(120).then(() => fetch(`http://localhost:3000/5000`)).then(r => r.text())
//   ]);

//   console.timeEnd('test');
//   console.log(ts);

// })()
// .catch(console.error);