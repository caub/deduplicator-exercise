const http = require('http');
const { PassThrough } = require('stream');


module.exports = function deduplicator() {

  const callbacks = new Map(); // map url => callbacks

  function trigger(url) { // execute callbacks for that url

    http.get(url, response => {
      response.setMaxListeners(1000);
      const cbs = callbacks.get(url);
      callbacks.delete(url);
      cbs.forEach(cb => {
        const pass = new PassThrough();
        cb(response.pipe(pass)); // pass the stream ('cloned' to allow multi-piping)
      });
    });
  }

  /**
   * deduplicator
   *  - url
   *  - callback with response as arg
   */
  return function deduplicate(url, cb) {
    const cbs = callbacks.get(url);
    // console.log('rec', url, 'found', cbs);

    if (!cbs) {
      callbacks.set(url, [cb]);
      setTimeout(trigger, 50*Math.random() + 1, url);
    } else {
      cbs.push(cb);
    }
  };
};