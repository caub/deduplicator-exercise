const deduplicator = require('./');
const { test } = require('tap')
const http = require('http');
const bl = require('bl');






test('basic callback request', function (t) {

  const server = http.createServer(function (req, res) {
    t.ok('request received');
    res.end('hello');
  }).listen(0, function () {

    const address = server.address();
    const dest = 'http://localhost:' + address.port;

    const deduplicate = deduplicator();
    deduplicate(dest, check);

    function check(response) {
      response.pipe(bl((err, data) => {
        t.error(err);
        t.deepEqual(data, Buffer.from('hello'));
        t.end();
      }));
    }
  });

  t.tearDown(server.close.bind(server))
})


test('deduplicate', function (t) {
  t.plan(5);

  const server = http.createServer(function (req, res) {
    t.ok('request received');
    res.end('hello');
  }).listen(0, function () {

    const address = server.address();
    const dest = 'http://localhost:' + address.port;

    const deduplicate = deduplicator();

    deduplicate(dest, check);
    deduplicate(dest, check);

    function check(response) {
      response.pipe(bl((err, data) => {
        t.error(err);
        t.deepEqual(data, Buffer.from('hello'));
      }));
    }
  })

  t.tearDown(server.close.bind(server))
})
