const http = require('http');
const deduplicate = require('./deduplicate')();

const MAIN_PORT = 3001;
const DEDUP_PORT = 3000;

// creating the slow server
http.createServer(function (req, res) {
  const num = req.url.slice(1);

  if (!isFinite(num)) {
    res.statusCode = 500;
    res.end();
    return;
  }

  setTimeout(function () {
    res.end('' + num);
  }, num);
})
.listen(MAIN_PORT, function () {
  console.log('slow server listening on', this.address().port)
});


// dedup server
http.createServer(function (req, res) {

  deduplicate(`http://localhost:${MAIN_PORT}${req.url}`, response => {
    response.pipe(res);
  });
})
.listen(DEDUP_PORT, function () {
  console.log('dedup server proxy listening on', this.address().port)
});

