# Deduplicate Exercise

This module allows you to deduplicate http calls. When calling a remote
HTTP server, it reduces the load by calling it once and have all
concurrent requests wait for the result.

Have a look at the code to know

## Example

```js
var deduplicate = require('./deduplicate')

deduplicate(url, function (err, data) {
  // do something with data
})
```

## Running the benchmark

In one terminal:

```
node example.js
```

In another terminal:

```
npm run bench
```

## The exercise

Your goal is to make the `deduplicate` function respect backpressure, like this:

```js
var deduplicate = require('./deduplicate')

deduplicate(url, function (err, data, cb) {
  // call cb() asynchronously
})
```

With this signature, `deduplicate` will keep returning the last
retrieved data until all `cb()` has been called for every incoming request.

Thanks to this change, you should be able to achieve 2x throughput.
